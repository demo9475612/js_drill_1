function countOlderCars(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }
    let carCount=0;
    for(let carId=0;inventory.length;carId++){
        if(inventory[carId].car_year < 2000){
            carCount++;
        }
    }
    return carCount;
    
}

module.exports = countOlderCars;