function getAllCarYears(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }
    const n=inventory.length;
    const carYear=new Array(n);
    for(let carId=0;carId<n;carId++){
        carYear[carId]=inventory[carId].car_year;
    }
    return carYear;
}

module.exports = getAllCarYears;