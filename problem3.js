function sortCarModelsAlphabetically(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }

    const carModels= [];
    for(let carId=0;carId<inventory.length;carId++){
        carModels.push(inventory[carId].car_model);
    }
    
    for(let i=0;i<carModels.length-1;i++){
        for(let j=i+1;j<carModels.length;j++){
            if(carModels[i].toLowerCase()>carModels[j].toLowerCase()){
                const tempCarModel=carModels[i];
                carModels[i]=carModels[j];
                carModels[j]=tempCarModel;
            }
        }
    }
    return carModels;
    
}

module.exports = sortCarModelsAlphabetically;