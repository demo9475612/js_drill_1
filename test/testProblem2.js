const getLastCar = require('../problem2');
const inventory = require('../inventory');

try {
    const lastCar = getLastCar(inventory);
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
} catch (error) {
    console.error(error.message);
}