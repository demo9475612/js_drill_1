function findCarById(inventory, id){
    if(!Array.isArray(inventory)){
        throw new Error('Invalid inventory Found');
    }
    for(let carId=0;inventory.length;carId++){
        if(inventory[carId].id===id){
            return inventory[carId];
        }
    }
    return null;
}
module.exports = findCarById;