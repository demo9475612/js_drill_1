function getLastCar(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }

    return inventory[inventory.length - 1];
}

module.exports = getLastCar;