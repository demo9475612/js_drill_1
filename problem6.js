function getBMWAndAudiCars(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }
    const BMWAndAudi=[];
    for(let carId=0;carId<inventory.length;carId++){
        if(inventory[carId].car_make === 'BMW' || inventory[carId].car_make === 'Audi'){
            BMWAndAudi.push(inventory[carId]);
        }
    }
    return BMWAndAudi;
}

module.exports = getBMWAndAudiCars;